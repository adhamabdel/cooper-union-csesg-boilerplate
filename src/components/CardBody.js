import React from 'react';
import Button from '@material-ui/core/Button';


class CardBody extends React.Component {
  render() {
    return (
      <div className="card-body">
        <p className="date">October 13 2018</p>

        <h2>{this.props.title}</h2>

        <p className="body-content">{this.props.text}</p>
<Button variant="containted" id="blogButtons"><a href="https://www.healthline.com/nutrition/why-is-coffee-good-for-you">Click to Find Out More</a> </Button>
      </div>
    )
  }
}
export default CardBody
