import React from 'react';
import Button from '@material-ui/core/Button';
import { connect } from "react-redux";
import { compose } from "redux";
import Card from './Card';


class BlogPage extends React.Component{
  render(){
return(
   <div>
   <Card details={{

     "category": "News",
     "title": "Coffee Known as Living Savior",
     "text": "After recent studies, many researchers found that drinking coffee once a day...",
     "image": "https://i.kinja-img.com/gawker-media/image/upload/s--Y2ZbGPfk--/c_scale,f_auto,fl_progressive,q_80,w_800/z9lgeuldllkazmau4d6t.jpg"
   }}></Card>
   <br />
   <Card details={{  "category": "Travel",
   "title": "Greek Food Brings Astonishing Health Benefits",
   "text": "We've discovered the amazing secret ingrediants in Greek food that will...",
   "image": "https://inm-baobab-prod-eu-west-1.s3.amazonaws.com/public/inm/media/image/iol/2018/07/16/16053179/greece%20-%20corfu1.jpg"
 }}>
   </Card>
   <br />
   <Card details={{ "category": "Desserts",
   "title": "This New Chooclate Lava Cake will have you begging for more",
   "text": "A small restaurant in Chicago has proved its...",
   "image": "https://d39ziaow49lrgk.cloudfront.net/wp-content/uploads/2016/02/Main-Image-Lava-Cake-.jpg"
 }}></Card>
   <br />
   <Card details={{ "category": "Dinner",
   "title": "This new Spanish chicken recipe is the newest craze!",
   "text": "Many families across the country are giving amazing ratings to this...",
   "image": "https://images-gmi-pmc.edge-generalmills.com/7ed1e04a-7ac6-4ca2-aa74-6c0938069062.jpg" }}></Card>
 </div>
)
}
};

export default BlogPage
